#Capturing image

import picamera

camera = picamera.PiCamera()

camera.capture(‘snapshot.jpg’)

#Sending image

import requests
from io import BytesIO
import base64

import json

with open("snapshot.jpg", "rb") as f:
    data = f.read()
    k=data.encode("base64")
o=str(k)
o=o[:-3]

o=o.replace(" ", "")
o=o.replace("\n", "")

print o
url = "http://orion.lab.fiware.org:1026/v2/entities/82763862537812099028718/attrs/image"
l = {"value": o ,"type": "String"}
headers = {'Content-Type' : 'application/json', 'X-Auth-Token': 's5udPe4jIp2l9i59yTEpvn83KIWbNZ'}
r = requests.put(url, headers=headers, json = l)
